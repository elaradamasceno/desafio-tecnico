window.datas = [];

function tabs(evt, nomeTab) {
    var i, tabcontent, tablinks;

    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }

    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    } 

    document.getElementById(nomeTab).style.display = "block";
    evt.currentTarget.className += " active";

	if (nomeTab == "historicoMoeda") {
		datas.forEach(function (elem, i) {			
			$("#tbody").append("<tr><td id='data'>" + elem.dia + "</td><td id='valor-conversao'>" + elem.valor);
		});		
	}
}