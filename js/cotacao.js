
var opcoes = document.querySelector("#valor-moedas");

opcoes.addEventListener('change', function(){
    var selecionado = this.value;
    //console.log(selecionado);

    const accessKey = "036ae2b738e1b3c7348235e42fbeff76";
    var moeda = selecionado;
    //var padrao = "USD"+moeda;

    var hoje = new Date();    

    //opções fixas do gráfico
    var opcoes = {
        title: {  text: '' },
        tooltip: {
            headerFormat: '<b>{series.name}</b><br />',
            pointFormat: '{point.y}'
        },

        xAxis: {
            categories: new Array(),
        },

        series: [{
            name: 'Cotações',
            data: new Array(), //valor moeda
        }]
    };

    var dfdteste = new $.Deferred();
    var promise = dfdteste.promise(); 

    for(var i = 6; i >= 0; i --){
        var _dia = new Date(hoje.getFullYear(), hoje.getMonth(), hoje.getDate() - i); 
        _dia = _dia.toISOString();
        _dia = _dia.split("T")[0];

        (function (i, _dia) {
            $.getJSON(
                "http://apilayer.net/api/historical?access_key="+accessKey+"&currencies="+moeda+"&source=USD&format=1&date=" + _dia,
                function(valor){
                    opcoes.xAxis.categories.push(_dia);
                    opcoes.series[0].data.push(valor.quotes['USD' + selecionado]);
                    
                    //console.log(valor.quotes['USD' + selecionado]);
                    //console.log(dia);
					
					window.datas.push({
						"dia": _dia,
						"valor": valor.quotes['USD' + selecionado]
					});
				
                    if(i == 0)
                        dfdteste.resolve();
                }
            );
        })(i, _dia);

    }
    promise.done(function(){
        Highcharts.chart('container', opcoes);	
    });   
});



